//schema for orders
const mongoose = require("mongoose");
const ordersSchema = mongoose.Schema({
    userId:
        {
        type: String,
        required: [true, "UserID required!"],
        
        },

    products: [
        {
            productId: {
                type: String
            },
            quantity: {
                type: Number
            }
        }
    ],
    totalAmount:{
        type: Number,
        required:[true,"Total amount required"]
    },
    purchasedOn:{
        type: Date,
        default: new Date()
    }
        
    


})

module.exports = mongoose.model("Order", ordersSchema);