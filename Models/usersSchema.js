//schema for user

const mongoose = require("mongoose");
const usersSchema = mongoose.Schema({
    email:{
        type: String,
        required: [true, "User e-mail required."]
    },
    password:{
        type: String,
        required: [true, "Password required."]
    },
    isAdmin:{
        type: Boolean,
        default: false
    }

})

module.exports = mongoose.model("User", usersSchema);