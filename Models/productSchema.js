//schema for products
const mongoose = require("mongoose");
const productSchema = mongoose.Schema({
    
    
    name:{
        type: String,
        required: [true, "Product name required."]
    },
    description:{
        type: String,
        required: [true, "Description Required."]
    },
    price:{
        type: Number,
        required: [true, "Price is required."]
    },
    isActive:{
        type: Boolean,
        default: true
    },
    createdOn:{
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Product", productSchema);