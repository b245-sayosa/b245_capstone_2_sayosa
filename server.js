const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/userRoutes.js");
const productRoutes = require("./Routes/productRoutes.js");
const ordersRoutes = require("./Routes/ordersRoutes.js");
const cartRoutes = require("./Routes/cartRoutes.js")



const port = 4000;
const app = express();
	mongoose.set('strictQuery', true);

	mongoose.connect("mongodb+srv://admin:admin@batch245-sayosa.g75wxqn.mongodb.net/batch245_Capstone_2_eshop?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})


	let db = mongoose.connection;

	//For error handling
	db.on("error", console.error.bind(console, "Connection Error!"));

	//For validation of the connection
	db.once("open", () => {console.log('Welcome to kdjShop. Please register.')});




// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());

//routing
app.use("/user", userRoutes);
app.use("/product", productRoutes);
app.use("/order", ordersRoutes);
// app.use("/cart", cartRoutes)



app.listen(port, ()=> console.log(`Server is running at Port ${port}`))