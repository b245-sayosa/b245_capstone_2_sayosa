const Product = require("../Models/productSchema.js");
const auth = require("../auth.js")


module.exports.addProduct = (request, response) =>{
	let input = request.body;
	let userData =auth.decode(request.headers.authorization)

	let newProduct = new Product({
		name: input.name,
		description: input.description,
		price: input.price
	});

	
	if(userData.isAdmin){
		return newProduct.save()
		.then(product =>{
			console.log(product);
			response.send(product);
		})
	
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}else{
		return response.send("You are not an admin!")
	}
	
}

module.exports.allProducts = async (request, response) => {

			await Product.find({})
			.then(result => response.send(result))
			.catch(error => response.send(error))
	}



module.exports.productDetails = (request, response) => {
	// to get the params from the url
	const productId = request.params.productId;

	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))

}

module.exports.updateProduct = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
    const input = request.body;

    if(!userData.isAdmin){
        return response.send("Not an admin.")
    }else{
        const product = Product.findById(productId)
            .then(result =>{
                if(result === null){
                    return response.send("Invalid Product ID!")
                 }else{
                    let updatedCourse = {
                        name: input.name,
                        description: input.description,
                        price: input.price
                    }
            
                    Product.findByIdAndUpdate(productId, archivedProduct, {new: true})
                    .then(result => {
                        console.log(result)
                        return response.send(result)
                        .catch(error => response.send(error))
                    })
                    }
                })
        }}
module.exports.archiveProduct = (request, response) => {
        const userData = auth.decode(request.headers.authorization);
        const productId = request.params.productId;
        const isActive = request.body.isActive
    
        if(!userData.isAdmin){
            return response.send("Not an admin.")
        }else{
            const product = Product.findById(productId)
                .then(result =>{
                    if(result === null){
                        return response.send("Invalid Product ID!")
                     }else{
                        
                        const archivedProduct = { isActive }
                    
                        
                
                    Product.findByIdAndUpdate(productId, archivedProduct, {new: true})
                    .then(result => {
                        console.log(result)
                        return response.send(result)
                        // .catch(error => response.send(error))
                    })
                    }
                })
        }}


        

   