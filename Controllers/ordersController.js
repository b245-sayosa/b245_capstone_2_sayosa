const Order = require("../Models/ordersSchema.js");
const auth = require("../auth.js")
const Product = require("../Models/productSchema.js");




module.exports.createOrder = (request, response) =>{
	let userData =auth.decode(request.headers.authorization)

	let newOrder = new Order(request.body)

	
	if(userData.isAdmin){
		return response.send("Admin not allowed to access")
		} else {
            return newOrder.save()
            .then(order =>{
                response.send(order)
            })

            .catch(error =>{
                response.send(error)
            })
        }
	
		
}
