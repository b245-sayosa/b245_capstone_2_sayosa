const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../Models/productSchema.js");

module.exports.userRegistration = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already taken!")
			}else{
				let newUser = new User({
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					
				})


				//save to database
				newUser.save()
				.then(save => {
					return response.send("You are now registered to our website!")
				})
				.catch(error => {
					return response.send(error)
				})





			}

		})
		.catch(error => {
			return response.send(error)
		})
    }


module.exports.userAuthentication = (request, response) => {
		let input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return response.send("Email is not yet registered. Register first before logging in!")
			}else{
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				}else{
					return response.send("Password is incorrect!")
				}

			}




		})
		.catch(error => {
			return response.send(error);
		})

}

module.exports.getAllUsers =  (request, response) => {

    User.find({})
   .then(result => response.send(result))
   .catch(error => response.send(error))
}
