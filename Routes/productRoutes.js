const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController.js");
const auth = require("../auth.js");


router.post("/", auth.verify, productController.addProduct);
router.get("/all", productController.allProducts)

router.get("/:productId", productController.productDetails);

router.put("/update/:productId", auth.verify, productController.updateProduct);

router.put("/archive/:productId", auth.verify, productController.archiveProduct);


module.exports = router;