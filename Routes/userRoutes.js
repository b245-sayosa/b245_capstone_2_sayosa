const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const userController = require("../Controllers/usersController.js")

// [Routes]


router.post("/register", userController.userRegistration);
router.post("/login", userController.userAuthentication);
router.get("/details",  userController.getAllUsers);

// router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);

module.exports = router;