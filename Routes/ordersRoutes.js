const express = require("express");
const router = express.Router();
const ordersController = require("../Controllers/ordersController.js");
const auth = require("../auth.js");




router.post("/", auth.verify, ordersController.createOrder);

module.exports = router;